class GoodsItem{

  num id;
  String thumbnailImgUrl;
  String goodsName;
  String goodsDetail;
  num goodsSalePercent;
  num goodsPrice;

  GoodsItem(this.id, this.thumbnailImgUrl, this.goodsName, this.goodsDetail, this.goodsSalePercent ,this.goodsPrice);

  factory GoodsItem.fromJson(Map<String, dynamic> json){
    return GoodsItem(
      json['id'],
      json['thumbnailImgUrl'],
      json['goodsName'],
      json['goodsDetail'],
      json['goodsSalePercent'],
      json['goodsPrice'],
    );
  }
}