import 'package:shop_accessory_app/model/goods_item.dart';


class GoodsListResult{
  String msg;
  num code;
  List<GoodsItem> list;
  num totalCount;

  GoodsListResult(this.msg, this.code , this.list , this.totalCount,);

  factory GoodsListResult.fromJson(Map<String, dynamic> json){
    return GoodsListResult(
      json['msg'],
      json['code'],
      json['list'] !=null ?
      (json['list'] as List).map((e) => GoodsItem.fromJson(e)).toList()
          : [],
      json['totalCount'],
    );
    // json으로 [{name:'dd'}, {name:'nn'} ]이런 String을 받아서 object로 List로 바꾸고 난
    //후에 어쩃든 List니깐 한 덩어리씩 던저줄 수 있다.
    // 한뭉치씩 던저줄때 그 이름이 e 이다스
    //다 작은 그릇으로 바꾼 후
    //리트로 정리
  }
}