

import 'package:easy_search_bar/easy_search_bar.dart';
import 'package:flutter/material.dart';
import 'package:shop_accessory_app/component/component_goods_item.dart';
import 'package:shop_accessory_app/model/goods_item.dart';
import 'package:shop_accessory_app/pages/page_goods_detail.dart';
import 'package:gif_view/gif_view.dart';
import 'package:shop_accessory_app/repository/repo_goods.dart';


class PageIndex extends StatefulWidget {
  const PageIndex({
    super.key});

  @override
  State<PageIndex> createState() => _PageIndexState();
}


class _PageIndexState extends State<PageIndex> with TickerProviderStateMixin{


  String searchValue = '';
  final List<String> _suggestions = ['귀걸이', '목걸이', '반지', '팔찌', '손목 시계', '브로치', '부토니에르', '벨트', '머리띠'];

  List<GoodsItem> _list = [];

  Future<void> _loadList() async {
    // 이 메서드가 repo 호출해서 데이터 받아온 다음에
    // setstate해서 _list 교체할거다. => oneway binding 이기 때문에 setState 실행
    // vue 의 경우 twoway binding 이기 때문에 교체된걸 따로 실행하지 않아도됨
    await RepoGoods().getGoods()
        .then((value) => {
          setState(() {
            _list = value.list;
          })
        })
        .catchError((err) => {
      debugPrint(err)
      });
  }

  @override
  void initState(){
    super.initState();
    _loadList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar:
        EasySearchBar(
        title: const Text('Example'
        ,style:TextStyle
            (color: Colors.white,fontSize: 20,fontFamily: 'Lemon', fontWeight:FontWeight.w200),
        ),
        backgroundColor: Colors.brown,
        onSearch: (value) => setState(() => searchValue = value),
        actions: [
          IconButton(icon: const Icon(Icons.person), onPressed: () {})
        ],
        iconTheme: IconThemeData(color: Colors.white),
        suggestions: _suggestions
    ),drawer: Drawer(
        child: ListView(
            padding: EdgeInsets.zero,
            children: [
              Container(
                height: 100,
              child: DrawerHeader(
                decoration: BoxDecoration(
                  color: Colors.brown,
                ),
                child: Text('메뉴',
                style: TextStyle(
                  fontSize: 20,
                  color: Colors.white,
                  fontFamily:'Lemon'
                ),),
              )),
              ListTile(
                  title: Text('회원 정보'),
                  onTap: () => Navigator.pop(context)
              ),
              ListTile(
                  title: Text(''),
                  onTap: () => Navigator.pop(context)
              )
            ]
        )
    ),
      body: _buildBody(context) );

  }


  Widget _buildBody(BuildContext context){
    return SingleChildScrollView(
      child: Column(
        children: [
          Container( margin: EdgeInsets.only(top : 10),
            child: Center(
              child: Text(' cold, smooth & tasty.',
                style:TextStyle(
                  fontSize: 25,
                  fontFamily: 'Lemon',
                  fontWeight: FontWeight.w500
                )),
            ),
          ),
          Container( margin: EdgeInsets.only(top: 20),
            child: GifView.asset('assets/dualipa.gif'
            ,controller: GifController(loop: false),)
          ),
          Container( margin: EdgeInsets.only(top: 20),
            child: Image.asset('assets/bigsale.png' ),
          ),
          Container(width: 400,
              child:
              GridView.builder(
                physics: ScrollPhysics(),
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
                itemCount: _list.length, //item 개수
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2, //1 개의 행에 보여줄 item 개수
                  childAspectRatio: 2 / 3, //item 의 가로 1, 세로 2 의 비율
                  mainAxisSpacing: 10, //수평 Padding
                  crossAxisSpacing: 10, //수직 Padding
                ),
                itemBuilder: (BuildContext context, int index) {
                  //item 의 반목문 항목 형성
                  return ComponentGoodsItem(
                      goodsItem: _list[index],
                      callback: (){
                        Navigator.of(context).push(MaterialPageRoute(builder: (context) =>
                            PageGoodsDetail(itemNum: _list[index])));
                      });
                },
              )),]
      )
    );
  }
}
