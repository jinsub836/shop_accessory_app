import 'package:flutter/material.dart';
import 'package:shop_accessory_app/component/component_goods_item.dart';
import 'package:shop_accessory_app/model/goods_item.dart';
import 'package:shop_accessory_app/repository/repo_goods.dart';

class GridviewPage extends StatefulWidget {
  const GridviewPage({Key? key}) : super(key: key);

  @override
  _GridviewPageState createState() => _GridviewPageState();
}

class _GridviewPageState extends State<GridviewPage> {

  List<GoodsItem> _list = [

  ];

  Future<void> _loadList() async {
    // 이 메서드가 repo 호출해서 데이터 받아온 다음에
    // setstate해서 _list 교체할거다. => oneway binding 이기 때문에 setState 실행
    // vue 의 경우 twoway binding 이기 때문에 교체된걸 따로 실행하지 않아도됨
    await RepoGoods().getGoods()
        .then((res) => {
      setState(() {
        _list = res.list;
      })
    })
        .catchError((err) => {
      debugPrint(err)
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('GridviewPage'),
        ),
        body: Container( width: 600 ,
          child:
        GridView.builder(
          itemCount: _list.length, //item 개수
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2, //1 개의 행에 보여줄 item 개수
            childAspectRatio: 2 / 3, //item 의 가로 1, 세로 2 의 비율
            mainAxisSpacing: 1, //수평 Padding
            crossAxisSpacing: 1, //수직 Padding
          ),
          itemBuilder: (BuildContext context, int index) {
            //item 의 반목문 항목 형성
            return ComponentGoodsItem(
                goodsItem: _list[index],
                callback: (){});
          },
        )),
    );
  }
}