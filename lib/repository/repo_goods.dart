import 'package:dio/dio.dart';
import 'package:shop_accessory_app/config/config_api.dart';
import 'package:shop_accessory_app/model/goods_list_result.dart';


class RepoGoods {
  Future<GoodsListResult> getGoods() async {
    Dio dio = Dio(); //대문자 = 빈생성자 호출

    print('1');

   final String _baseUrl ='$apiUri/accessorises-goods/all';//엔드포인트 = 주소
    // const , final 차이  const는 컴파일 이전에 값이 들어가고 final은 컴파일 후에 값이 들어감
   final response = await dio.get(
     _baseUrl,
     options: Options(
       followRedirects: false,
       validateStatus: (status) {
          return status == 200;
         }
        )
   );
   
   print('2');

   return GoodsListResult.fromJson(response.data);
  }
}